﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;
using System.Data.Entity;
using LVL2_ASPNet_MVC_45.Models;

namespace LVL2_ASPNet_MVC_45.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeEntity1 db = new EmployeeEntity1();
        
        public ActionResult Index()
        {
            var E = db.Employees.ToList();
            return View(E);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if(ModelState.IsValid)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                { 
                    try
                    {
                        db.Employees.Add(employee);
                        db.SaveChanges();

                        if (employee.Id > 0)
                        {
                            int a = 0;
                            int total = 10 / a;
                            TSalary salary = new TSalary();
                            salary.Id_Employee = employee.Id;
                            salary.Salary = employee.Salary;
                            db.TSalaries.Add(salary);
                            db.SaveChanges();
                            transaction.Commit();
                        }
                        return RedirectToAction("Index");
                    }
                    catch (Exception Ex)
                    {
                        transaction.Rollback();
                        ViewData["data"] = Ex.Message;
                    }
                }
            }
            return View();
        }
   
        public ActionResult Details(int id)
        {
            using (EmployeeEntity1 dbModel = new EmployeeEntity1())
            {
                return View(dbModel.Employees.Where(x => x.Id == id).FirstOrDefault());
            }
                
        }

        public ActionResult Edit(int id)
        {
            Employee employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            return View(employee);
        }

        [HttpPost]
        public ActionResult Edit(int id,Employee employee)
        {
            try
            {
                db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                
            }
            return View();
        }
        public ActionResult Delete(int id)
        {
            Employee employe = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            return View(employe);
        }
        [HttpPost]
        public ActionResult Delete(int id, Employee employee)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    TSalary s = db.TSalaries.Where(x => x.Id_Employee == id).FirstOrDefault();
                    db.TSalaries.Remove(s);
                    db.SaveChanges();

                    Employee e = db.Employees.Where(x => x.Id == id).FirstOrDefault();
                    db.Employees.Remove(e);
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
           
        }

    }
}